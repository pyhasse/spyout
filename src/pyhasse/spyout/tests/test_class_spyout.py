from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.core.order import Order
from pyhasse.chain.calc import Chain
from pyhasse.spyout.calc import Spyout

import os
import pytest


@pytest.fixture
def data():
    TESTFILENAME = "/data/spyout_test.txt"
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath + TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    order = Order(matrix.data, matrix.rows, matrix.cols)
    zeta = order.calc_relatmatrix(matrix.data, matrix.rows, matrix.cols)
    covd, cov = order.calc_cov(zeta, matrix.rows)
    chain = Chain(matrix, csv, cov)
    ds = order.calc_downset(zeta, matrix.rows)
    us = order.calc_upset(zeta, matrix.rows)
    iset = order.calc_incompset(zeta, matrix.rows)
    spy = Spyout(csv, matrix)
    return csv, matrix, order, zeta, covd, cov, chain, spy, ds, us, iset


def test_dim_matrix(data):
    csv = data[0]

    assert csv.rows == 9
    assert csv.cols == 4
    assert csv.prop == ["q1", "q2", "q3", "q4"]
    assert csv.obj == ["a", "b", "c", "d", "e", "f", "g", "h", "i"]
    result = [2, 3, 4, 5]  # cm[0]
    for i in range(len(result)):
        assert "{0:.2f}".format(result[i]) == "{0:.2f}".format(csv.data[0][i])
    assert csv.data[1] == [5, 4, 3, 2]


def test_characteristics(data):
    spy = data[7]
    cm = spy.calc_matrixcharacteristics(4, 2)
    assert cm[0] == [10, 10, 8, 8]
    assert cm[1] == [1, 1, 1, 1]
    result = [4.67, 4.78, 4.00, 3.67]  # cm[2]
    for i in range(len(result)):
        assert "{0:.2f}".format(result[i]) == "{0:.2f}".format(cm[2][i])
    result = [2.96, 2.68, 2.29, 2.65]  # cm[3]
    for i in range(len(result)):
        assert "{0:.2f}".format(result[i]) == "{0:.2f}".format(cm[3][i])


def test_downupincomp(data):
    """downset, upset, incompset-counts"""
    matrix = data[1]
    ds = data[8]
    us = data[9]
    iset = data[10]
    cds = []
    cus = []
    cincomp = []
    for i in range(0, matrix.rows):
        cds.append(len(ds[i]))
        cus.append(len(us[i]))
        cincomp.append(len(iset[i]))
    assert cds == [1, 1, 2, 1, 1, 1, 1, 1, 9]
    assert cus == [2, 3, 2, 2, 2, 2, 2, 2, 1]
    assert cincomp == [7, 6, 6, 7, 7, 7, 7, 7, 0]
    # spectrum for each object in objred (cds[i], cus[i], cincomp[i])
    spec = []
    for ob in matrix.obj:
        spec.append(0)
        iob = matrix.obj.index(ob)
        spec[iob] = (cds[iob], cus[iob], cincomp[iob])
    assert spec[0] == (1, 2, 7)
    assert spec[1] == (1, 3, 6)
    assert spec[2] == (2, 2, 6)
    assert spec[8] == (9, 1, 0)


def test_chainlist(data):
    matrix = data[1]
    order = data[2]
    zeta = data[3]
    chain = data[6]
    spy = data[7]
    levobj = order.calc_level(zeta, matrix.rows)
    nr_lev = len(levobj)
    levmax = 0
    for i in range(0, len(levobj)):
        if len(levobj[i]) > levmax:
            levmax = len(levobj[i])
    assert levmax == 7
    assert matrix.obj == ["a", "b", "c", "d", "e", "f", "g", "h", "i"]
    startxn = "i"  # user_input
    sinkxn = "b"  # user_input
    startx = matrix.obj.index(startxn)
    sinkx = matrix.obj.index(sinkxn)
    npath, flagch = chain.connectivity(
        startx, sinkx
    )
    comparable = False
    if npath:
        comparable = True
        assert npath == [[8, 2, 1]]
        container = spy.chaincore(matrix.obj, npath, nr_lev)
        assert container == [["i", "c", "b"]]
    else:
        assert comparable is False


def test_conflict(data):
    csv = data[0]
    matrix = data[1]
    spy = data[7]

    cm = spy.calc_matrixcharacteristics(4, 2)
    startx = "d"  # userinput
    sinkx = "e"  # userinput
    txt, countconflict = spy.conflict(
        matrix.data, cm, matrix.obj, csv.prop, csv.cols, startx, sinkx
    )
    assert countconflict == 2
