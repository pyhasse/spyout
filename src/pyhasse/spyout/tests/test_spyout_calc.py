import os
import pytest

from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix


@pytest.fixture
def data():
    TESTFILENAME = "/data/example_r6_k4.csv"
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath + TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    return csv, matrix


def test_spyout(data):
    """Test all operations in calc"""

    matrix = data[1]

    assert matrix.prop == ["q1", "q2", "q3", "q4"]
    assert matrix.obj == ["a", "b", "c", "d", "e", "f"]
    assert len(matrix.obj) == 6
