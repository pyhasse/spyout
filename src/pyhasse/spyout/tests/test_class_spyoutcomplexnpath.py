import os
import pytest
from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.core.order import Order
from pyhasse.spyout.calc import Spyout


@pytest.fixture
def data():
    TESTFILENAME = "/data/spyout_test.txt"
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath + TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    order = Order(matrix.data, matrix.rows, matrix.cols)
    zeta = order.calc_relatmatrix(matrix.data, matrix.rows, matrix.cols)
    covd, cov = order.calc_cov(zeta, matrix.rows)
    spy = Spyout(csv, matrix)
    return matrix, csv, spy


def test_chainlist(data):
    spy = data[2]

    nr_lev = 5
    objred = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"]

    npath = [[1, 4, 5], [1, 3, 5], [1, 7, 8, 6, 5], [1, 9, 5], [1, 5],
             [1, 7, 9, 10, 5]]
    comparable = False
    if npath:
        comparable = True
        container = spy.chaincore(objred, npath, nr_lev)
        print("container= ", container)
        assert len(container[0]) == 5
        assert len(container[1]) == 5
        assert len(container[2]) == 3
        assert len(container[3]) == 3
        assert len(container[4]) == 3
        assert len(container[5]) == 2
    else:
        assert comparable is False
