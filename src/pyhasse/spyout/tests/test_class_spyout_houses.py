from pyhasse.core.csv_io import CSVReader
from pyhasse.core.matrix import Matrix
from pyhasse.core.order import Order
from pyhasse.chain.calc import Chain
from pyhasse.spyout.calc import Spyout

import os
import pytest


@pytest.fixture
def data():
    TESTFILENAME = "/data/houses.txt"
    datapath, filename = os.path.split(__file__)
    csv = CSVReader(fn=datapath + TESTFILENAME, ndec=3)
    matrix = Matrix(csv.data, csv.obj, csv.prop, reduced=True)
    order = Order(matrix.data, matrix.rows, matrix.cols)
    zeta = order.calc_relatmatrix(matrix.data, matrix.rows, matrix.cols)
    ds = order.calc_downset(zeta, matrix.rows)
    us = order.calc_upset(zeta, matrix.rows)
    iset = order.calc_incompset(zeta, matrix.rows)
    covd, cov = order.calc_cov(zeta, matrix.rows)
    chain = Chain(matrix, csv, cov)
    spy = Spyout(csv, matrix)
    return csv, matrix, order, zeta, \
        cov, covd, chain, spy, ds, us, iset


def test_dim_matrix(data):
    csv = data[0]
    assert csv.rows == 15
    assert csv.cols == 8


def test_characteristics(data):
    spy = data[7]
    # cm is the matrix of ...
    # max, min,mean, std... of each column
    cm = spy.calc_matrixcharacteristics(5)
    assert cm[0] == [
        5.0,
        360.0,
        3.0,
        4.0,
        4.0,
        9.0,
        4.0,
        1.0,
    ]  # first row of cm (maximal values of each column of data matrix)
    assert cm[1] == [
        1,
        80.0,
        1,
        1,
        0,
        4,
        0,
        0,
    ]  # second row of cm (minimal values of each column of the data matrix)


def test_downupincomp(data):
    """ downset, upset, incompset-counts"""
    matrix = data[1]
    ds = data[8]
    us = data[9]
    iset = data[10]
    rred = matrix.rows
    cds = []
    cus = []
    cincomp = []
    for i in range(0, rred):
        cds.append(len(ds[i]))
        cus.append(len(us[i]))
        cincomp.append(len(iset[i]))
    assert cds == [5, 1, 1, 4, 9, 1, 1, 1, 1, 1, 4, 1, 5, 7, 5]
    assert cus == [1, 2, 2, 1, 1, 7, 6, 2, 6, 7, 1, 5, 3, 1, 2]
    assert cincomp == [10, 13, 13, 11, 6, 8, 9, 13, 9, 8, 11, 10, 8, 8, 9]
    # spectrum for each object in objred (cds[i], cus[i], cincomp[i])
    spec = []
    for ob in matrix.obj:
        spec.append(0)
        iob = matrix.obj.index(ob)
        spec[iob] = (cds[iob], cus[iob], cincomp[iob])
    assert spec[0] == (5, 1, 10)
    assert spec[1] == (1, 2, 13)
    assert spec[2] == (1, 2, 13)
    assert spec[14] == (5, 2, 9)


def test_chainlist(data):
    matrix = data[1]
    order = data[2]
    zeta = data[3]
    chain = data[6]
    spy = data[7]

    lev_obj = order.calc_level(zeta, matrix.rows)
    levmax = 0
    for i in range(0, len(lev_obj)):
        if len(lev_obj[i]) > levmax:
            levmax = len(lev_obj[i])
    assert levmax == 6
    startn = "a14"  # user_input
    sinkn = "a7"  # user_input
    npath, flagch = chain.connectivity(startn, sinkn)
    comparable = False
    nr_lev = len(lev_obj)
    if npath:
        comparable = True
        assert npath == [[13, 12, 6]]
        container = spy.chaincore(matrix.obj, npath, nr_lev)
        assert container == [["a14", "a13", "a7"]]
    else:
        assert comparable is False
