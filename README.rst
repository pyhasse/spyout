======================
Module: pyhasse.spyout
======================

* Free software: MIT license
* Documentation: https://pyhasse.org


Features
--------

Calculate:

- Statistical characteristics
- Connectivities
- List of chains
- Conflicts
